provider "aws" {
  region     = "ap-south-1"
  access_key = var.access_key
  secret_key = var.secret_key
}
module "EC2" {
  source = "./modules/EC2"
  key = file("${path.module}/id_rsa.pub")
  image_id = "${var.image_id}"
  instance_type = "${var.instance_type}"
  key_name = "${var.key_name}" 
  aws_security_group = module.sg.vpc_security_group_ids
  depends_on = [
    module.sg
  ]
}
module "sg" {
  source = "./modules/sg"
  ports = var.ports
}
output mypublicIp {
  value = module.EC2.publicIP
}